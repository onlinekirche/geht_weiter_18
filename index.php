<?php
session_start();
?>
<!DOCTYPE html>
<html lang="de">

  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Weihnachten geht weiter ist ein Projekt der Evangelischen Kirche Mitteldeutschlands">
    <meta name="keywords" content="Weihnachten, Kirche, Evangelisch, Ev, Evang, Mitteldeutschland, Christus, Jesus, Protestant, protestantisch, EKM, EKMD, Glauben, lutherisch">
    <meta name="author" content="OnlineKirche, Rob Schwarz, Joerg Sorge">
    <!-- <meta http-equiv="cache-control" content="no-cache" />-->

    <meta property="og:title" content="Weihnachten geht weiter" />
    <meta property="og:description" content="Ein Projekt der Evangelischen Kirche in Mitteldeutschland" />
    <meta property="og:type" content="website" />
    <meta property="og:url" content="https://onlinekirche.net/gehtweiter" />

    <meta property="og:image" content="https://onlinekirche.net/gehtweiter/img/og_img.jpg" />
    <meta property="og:image:secure_url" content="https://onlinekirche.net/gehtweiter/img/og_img.jpg" />
    <meta property="og:image:type" content="image/jpeg" />
    <meta property="og:image:width" content="1040" />
    <meta property="og:image:height" content="950" />
    <meta property="og:image:alt" content="Maria und Joseph als Comic Figuren" />

    <title>Weihnachten geht weiter</title>

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="https://fonts.googleapis.com/css?family=Catamaran:100,200,300,400,500,600,700,800,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lato:100,100i,300,300i,400,400i,700,700i,900,900i" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/one-page-wonder.css" rel="stylesheet">
    <link href="css/one-page-wonder-custom.css" rel="stylesheet">

  </head>

  <body>
      <!-- Bootstrap core JavaScript -->
      <!-- Must load before commentics -->
      <script src="vendor/jquery/jquery.min.js"></script>
      <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-dark navbar-custom fixed-top">
      <div class="container">
        <a class="navbar-brand" href="#">Home</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item">
              <a class="nav-link" href="#episode1">Episode 1</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#episode2">Episode 2</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#episode3">Episode 3</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#episode4">Episode 4</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#episode5">Episode 5</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#comment">Kommentieren</a>
            </li>
          </ul>
        </div>
      </div>
    </nav>

    <header class="masthead text-center text-white">
      <div class="masthead-content">
        <div class="container">
            <h1 class="masthead-heading"> <br>  </h1>
            <h1 class="masthead-heading"> <br>  </h1>
            <h1 class="masthead-heading"> <br>  </h1>
            <h1 class="masthead-heading"> <br>  </h1>
            <h1 class="masthead-heading mb-0">Weihnachten geht weiter</h1>
            <h1 class="masthead-heading"> </h1>
        </div>
      </div>
    </header>

    <section>
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
            <div class="p-5">
                <h2 class="text-center">"Weihnachten geht weiter"</h2>
                <p class="text-center"><br>Der junge Videokünstler <a href="https://www.rob-schwarz.de" target="_blank">Rob Schwarz</a> hat im Auftrag der EKM fünf avantgardistische Kurzfilme produziert. <br>
                    Sie setzen sich mit der Frage auseinander, <br>
                    wie die Botschaft von Weihnachten heute verstanden werden und weiter gehen kann.</p>

                <p class="text-center">Kommentare gerne direkt hier oder auf unserer <img class="img-fluid" src="img/social_facebook_box_blue.32.png" alt="Logo facebook">
                  <a href="https://www.facebook.com/ekmd.de" target="_blank">facebook-Seite</a>
                  und auf <img class="img-fluid" src="img/IG_Glyph_Fill.png" alt="Logo Instagram">
                  <a href="https://www.instagram.com/EKMbilder/" target="_blank">Instagram</a>
                </p>
            </div>
          </div>
        </div>
      </div>
    </section>

    <section>
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
            <div class="p-5">
                <p class="text-center">
                    Die Clips spielen in einer phantastischen Kunstwelt und basieren auf traditionellen (erzgebirgischen) Weihnachtsutensilien. <br>
                    Nüsse, Räuchermännchen, Pyramiden, Kerzen und Nussknacker verkörpern die Figuren der biblischen Weihnachtsgeschichte und müssen ihre Welt gegen aggressiv aufdringliche Weihnachtsmänner und Konsumtrubel verteidigen. <br>
                    In welcher Figur erkenne ich mich wieder?
                </p>
                <p class="text-center">
                    Veröffentlichung der Clips ist jeweils am Freitag vor den Adventssonntagen sowie am Heiligabend, immer um 12 Uhr mittags.
                </p>
            </div>
          </div>
        </div>
      </div>
    </section>

    <section id="playlist">
      <div class="container bg-alt-3">
        <div class="row">
          <div class="col-lg-12">
            <div class="p-5">
              <h2 class="display-4 text-center fg-alt-3">Playlist</h2>
              <h3 class="text-center fg-alt-3">Alle bisher veröffentlichten Episoden</h3>
              <div class="embed-responsive embed-responsive-16by9">
                  <iframe width="560" height="315" src="https://www.youtube.com/embed/videoseries?list=PLAai1Tn0apnOYcXVx_96NZMcQB9cqytqG" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>

    <section>
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
            <div class="p-5">

            </div>
          </div>
        </div>
      </div>
    </section>

    <section id="episode1">
      <div class="container bg-alt-3">
        <div class="row">
          <div class="col-lg-12">
            <div class="p-5">
              <h2 class="display-4 text-center fg-alt-3">Episode 1</h2>
              <h4 class="text-center fg-alt-3">Veröffentlichung Freitag 30.11.18 - 12 Uhr</h4>
              <p class="text-center fg-alt-3">
                  <a href="https://www.bibleserver.com/text/LUT/Lukas2,1-6" target="_blank">Lukas 2, 1-6</a>
              </p>
              <p class="text-center">
                  <a href="https://youtu.be/SRhsPbb3mFg">Clip 1 ohne Text</a>
              </p>
              <div class="embed-responsive embed-responsive-16by9">
                  <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/rzxW96bbbxE" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>

    <section>
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
            <div class="p-5">

            </div>
          </div>
        </div>
      </div>
    </section>

    <section id="episode2">
      <div class="container bg-alt-3">
        <div class="row">
          <div class="col-lg-12">
            <div class="p-5">
              <h2 class="display-4 text-center fg-alt-3">Episode 2</h2>
              <h4 class="text-center fg-alt-3">Veröffentlichung Freitag 07.12.18 - 12 Uhr</h4>
              <p class="text-center fg-alt-3">
                  <a href="https://www.bibleserver.com/text/LUT/Lukas2,8-15" target="_blank">Lukas 2, 8-15</a>
              </p>
              <p class="text-center">
                  <a href="https://youtu.be/MVOXUNHGQOw">Clip 2 ohne Text</a>
              </p>
              <div class="embed-responsive embed-responsive-16by9">
                  <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/uJmM3UuTlUA" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
              </div>
            </div>
          </div>
      </div>
      </div>
    </section>

    <section>
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
            <div class="p-5">

            </div>
          </div>
        </div>
      </div>
    </section>

    <section id="episode3">
      <div class="container bg-alt-3">
        <div class="row">
          <div class="col-lg-12">
            <div class="p-5">
              <h2 class="display-4 text-center fg-alt-3">Episode 3</h2>
              <h4 class="text-center fg-alt-3">Veröffentlichung Freitag 14.12.18 - 12 Uhr</h4>
              <p class="text-center fg-alt-3">
                  <a href="https://www.bibleserver.com/text/LUT/Matthäus2,1-2" target="_blank">Matthäus 2, 1-2</a>
              </p>
              <p class="text-center">
                  <a href="https://youtu.be/eXirRNGpO1o">Clip 3 ohne Text</a>
              </p>
              <div class="embed-responsive embed-responsive-16by9">
                  <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/JHjvASPVT44" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
              </div>
            </div>
          </div>
      </div>
      </div>
    </section>

    <section>
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
            <div class="p-5">

            </div>
          </div>
        </div>
      </div>
    </section>

    <section id="episode4">
      <div class="container bg-alt-3">
        <div class="row">
          <div class="col-lg-12">
            <div class="p-5">
              <h2 class="display-4 text-center fg-alt-3">Episode 4</h2>
              <h4 class="text-center fg-alt-3">Veröffentlichung Freitag 21.12.18 - 12 Uhr</h4>
              <p class="text-center fg-alt-3">
                  <a href="https://www.bibleserver.com/text/LUT/Matthäus2,3-6" target="_blank">Matthäus 2, 3-6</a>
              </p>
              <p class="text-center">
                  <a href="https://youtu.be/ZEmrtxLv3kQ">Clip 4 ohne Text</a>
              </p>
              <div class="embed-responsive embed-responsive-16by9">
                  <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/ZKJvfZHdR60" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
              </div>
            </div>
          </div>
      </div>
      </div>
    </section>

    <section>
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
            <div class="p-5">

            </div>
          </div>
        </div>
      </div>
    </section>

    <section id="episode5">
      <div class="container bg-alt-3">
        <div class="row">
          <div class="col-lg-12">
            <div class="p-5">
              <h2 class="display-4 text-center fg-alt-3">Episode 5</h2>
              <h4 class="text-center fg-alt-3">Veröffentlichung Montag 24.12.18 - 12 Uhr</h4>
              <p class="text-center fg-alt-3">
              <a href="https://www.bibleserver.com/text/LUT/Lukas2,16-20" target="_blank">Lukas 2, 16-20</a>
              </p>
              <p class="text-center">
                  <a href="https://youtu.be/p31LFIjVuhY">Clip 5 ohne Text</a>
              </p>
              <div class="embed-responsive embed-responsive-16by9">
                  <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/yHyHt7HNV5w" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
              </div>
            </div>
          </div>
      </div>
      </div>
    </section>

    <section id="comment">
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
            <div class="p-5">
              <p class="text-center">
                <?php
                $cmtx_identifier = '3';
                $cmtx_reference  = 'gehtweiter';
                $cmtx_folder     = '/inc-cm-9b9df53e6e895e2cea/';
                require($_SERVER['DOCUMENT_ROOT'] . $cmtx_folder . 'frontend/index.php');
                ?>
            </p>
            </div>
          </div>
       </div>
      </div>
    </section>

    <section>
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
            <div class="p-5">
              <p class="text-center">Ein Projekt der <a href="https://ekmd.de" target="_blank">Evangelischen Kirche in Mitteldeutschland</a><br>
              mit Unterstützung der <a href="https://onlinekirche.ekmd.de" target="_blank">OnlineKirche</a>, <br>
                  ein Erprobungsraum der Evangelischen Kirche in Mitteldeutschland.</p>
              <p class="text-center"><a href="https://onlinekirche.ekmd.de/service/impressum/" target="_blank">Impressum</a><br>
              <a href="https://onlinekirche.ekmd.de/service/datenschutz/" target="_blank">Datenschutz</a></p>
            </div>
          </div>
       </div>
      </div>
    </section>

    <!-- Footer -->
    <footer class="py-5 bg-black">
      <div class="container">
        <p class="m-0 text-center text-white small"><a href="https://github.com/BlackrockDigital/startbootstrap-one-page-wonder" target= "_blank">Credits to BlackrockDigital</a></p>
      </div>
      <!-- /.container -->
    </footer>

  </body>
</html>
